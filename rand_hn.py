import requests
from random2 import randint
import sys

BASE_URL = 'https://hacker-news.firebaseio.com/v0'

def main():
    link = getRandomStoryLink()
    print(link)

def getStoryIds():
    response = requests.get(BASE_URL + '/topstories.json?print=pretty')
    storyIds = response.json()
    return storyIds[:50]

def getRandomStoryLink():
    ids = getStoryIds()
    id = getRandomId(ids)
    return getStoryFromId(id)

def getStoryFromId(id):
    url = BASE_URL + '/item/' + str(id) + '.json?print=pretty'
    response = requests.get(url)
    return response.json()["url"]

def getRandomId(ids):
    idx = randint(0,49)
    return ids[idx]

if __name__ == '__main__':
    sys.exit(main())
