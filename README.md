### Random Hacker News

Provides a link to one of the top 50 stories on Hacker News. Inspired by [Quiet Hacker News](https://quiethn.com/)

## Usage

1. Clone this repo.
2. Install pipenv - [instructions](https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv)
3. Create a virtual environment using the command `pipenv shell`.
4. Execute `python3 rand_hn.py`
